# External Proofs

While the Verifiable Credential (VC) model provides a mean to have machine readable claims, the value of the claims could point to unstructured documents such as video, image, PDFs, ...

To ease the verification of those unstructured document, we recommend to convert those to [PDF/A-3a](https://www.iso.org/standard/57229.html) and embed structured machine readable metadata using [XMP](https://www.adobe.com/devnet/xmp.html) format.

Each type of claim will have to define its specific [Domain Specific Language (DSL)](https://en.wikipedia.org/wiki/Domain-specific_language), like for invoices[^facturx]

[^facturx]: [Factur-X](https://fnfe-mpe.org/factur-x/factur-x_en/)
