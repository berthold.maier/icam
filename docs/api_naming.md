# API endpoint naming convention

## Gaia-X Registry

The Gaia-X Registry is the equivalent of the [Verifiable Data Registry](https://www.w3.org/TR/vc-data-model/#dfn-verifiable-data-registries) as defined in the Verifiable Credential model.

The Gaia-X Registry is governs by the rules set by the bodies (Commitees and Working Groups) of Gaia-X association.

To foster collaboration and alignment between different initiaves and Gaia-X partners, the Gaia-X Registry API endpoints naming is based on the [European Blockchain Services Infrastructure (EBSI)](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSI/Home) naming conventions.


Please refer to the corresponding API documentation (OpenAPI/Swagger) from the EBSI for the following resources:

- Gaia-X schema: https://api-pilot.ebsi.eu/docs/apis/trusted-schemas-registry/latest#/
- Gaia-X shapes (SHACL files) is adapted from the above schema API as follow:
    ```shell
    /trusted-shape-registry/v1/shapes
    /trusted-shape-registry/v1/shapes/{shapeId}
    ```
- Gaia-X Compliance issuers (the GXDCHs public keys): https://api-pilot.ebsi.eu/docs/apis/trusted-issuers-registry/latest#/
- Gaia-X Compliance services (the GXDCH instances endpoints): https://api-pilot.ebsi.eu/docs/apis/trusted-apps-registry/latest#/


<details>
<summary> Getting list of schema from the Gaia-X Registry</summary>

```shell
GET https://registry.gaia-x.eu/api/<version>/trusted-schemas-registry/v2/schemas
```

</details>
