# System Features

## SSI Back Channel login

### Description

The feature provides the capability to login over an QR code, and an SSI
Backchannel provided by the trust services API. This provider enables
the user to use his personal SSI wallet for login to a protected
resource. The provider itself must be configured over a standard oidc
identity provider configuration within an IAM System.

### Stimulus/Response Sequences

#### User Authentication

The purpose of this flow is the authentication of the user and retrieval
of trustworthy identity data. The flow starts with an application
(Relying Party) which initiates OpenID Connect authorization flow
through its internal IAM system, requesting normal OIDC scopes as well
as specific Gaia-X scopes, which would be translated into a set of
proofs requested from the holder and its organization. The result of
this flow is an authenticated identity as well as a set of claims
resulting from the given set of proofs. The proof request and response
process will be conducted by Trust Services API
[\[IDM.TSA\]](#references).

![diagram](media/image5.png)

**Figure** **4**: SSI Back Channel login

#### User Authentication roaming between two applications and security domains

The purpose of this flow is the authentication and authorization with
the same user identity when moving between two applications and security
domains. This flow also illustrates how one application may dynamically
gain access to resources hosted with another application in a
trustworthy manner.

![diagram](media/image6.png)

**Figure** **5**: Cross domain DIDComm-based Front-Channel
Authentication

### Functional Requirements

<table>
<tbody>
<tr class="odd">
<td><em>Functional Requirement</em></td>
<td><em>Protocol</em></td>
<td><em>Parallelism</em></td>
</tr>
<tr class="even">
<td><a href="#5kk3fbdrbn0d"><em><u>[IDM.AA.00014] Credential Based Access Control (CrBac…</u></em></a></td>
<td>Trust Service API</td>
<td><p>Multi-user</p>
<p>Multi-session</p></td>
</tr>
<tr class="odd">
<td><a href="#njox283x6qs4"><em><u>[IDM.AA.00016] Standard open source IAM Package</u></em></a></td>
<td>OIDC</td>
<td><p>Multi-user</p>
<p>Multi-session</p></td>
</tr>
<tr class="even">
<td><a href="#vpjlfxj8j5mn"><em><u>[IDM.AA.00017] OpenID Provider Configuration Information</u></em></a></td>
<td>OIDC</td>
<td></td>
</tr>
<tr class="odd">
<td><a href="#lkn9j1lwgys"><em><u>[IDM.AA.00018] OpenID Connect Implicit profile</u></em></a></td>
<td>OIDC</td>
<td><p>Multi-user</p>
<p>Multi-session</p></td>
</tr>
<tr class="even">
<td><a href="#fynehgnmpqed"><em><u>[IDM.AA.00019] OAuth 2.0 Security Best…</u></em></a></td>
<td>OIDC</td>
<td><p>Multi-user</p>
<p>Multi-session</p></td>
</tr>
<tr class="odd">
<td><a href="#ojv09cuzys85"><em><u>[IDM.AA.00020] SSI Login Page</u></em></a></td>
<td>HTTP, Trust Service API</td>
<td><p>Multi-user</p>
<p>Multi-session</p></td>
</tr>
<tr class="even">
<td><a href="#litweym7qo93"><em><u>IDM.AA.00021] QR Code Generation</u></em></a></td>
<td>Trust Service API</td>
<td><p>Multi-user</p>
<p>Multi-session</p></td>
</tr>
<tr class="odd">
<td><a href="#57agswltgke5"><em><u>[IDM.AA.00022] Login State Background Polling</u></em></a></td>
<td>HTTP</td>
<td><p>Multi-user</p>
<p>Multi-session</p></td>
</tr>
<tr class="even">
<td><a href="#9nzshg3udpzd"><em><u>[IDM.AA.00023] Session Handling</u></em></a></td>
<td>Trust Service API</td>
<td><p>Multi-user</p>
<p>Multi-session</p></td>
</tr>
</tbody>
</table>

**Table** **4**: Functional Requirements SSI Back Channel login

## IAT Issuing

### Description 

The feature is handled over the IAT Provider which checks in the
background over policies the trust relationship before the issuing of an
Initial Access Token (IAT). The IAT can be used later for a dynamic
client registration as defined in [<u>\[RFC7591\]</u>](#gvlloyaprumi).
The IAT can also be used for other purposes later, because this IAT is
bound to a proof request. The sequence diagram below shows the basic
concept of how to. The process/feature MAY be modified if there are
security considerations or any other optimization. The final how to,
MUST be described in the final concepts.

### Stimulus/Response Sequences

![diagram](media/image7.png)

**Figure** **6**: IAT Issuing

### Functional Requirements

<table>
<tbody>
<tr class="odd">
<td><em>Functional Requirement</em></td>
<td><em>Protocol</em></td>
<td><em>Parallelism</em></td>
</tr>
<tr class="even">
<td><a href="#5kk3fbdrbn0d"><em><u>[IDM.AA.00014] Credential Based Access Control (CrBac…</u></em></a></td>
<td>Trust Service API</td>
<td><p>Multi-client</p>
<p>Multi-session</p></td>
</tr>
<tr class="odd">
<td><a href="#9u0liim6r122"><em><u>[IDM.AA.00025] Policy based authorization</u></em></a></td>
<td>Trust Service API</td>
<td><p>Multi-client</p>
<p>Multi-session</p></td>
</tr>
<tr class="even">
<td><a href="#99uitfty1mzv"><em><u>[IDM.AA.00026] Standard IAM Compatibility</u></em></a></td>
<td>OAuth2</td>
<td><p>Multi-client</p>
<p>Multi-session</p></td>
</tr>
<tr class="odd">
<td><a href="#klb58aoyw9a3"><em><u>[IDM.AA.00027] Client Registration</u></em></a></td>
<td>OAuth2</td>
<td><p>Multi-client</p>
<p>Multi-session</p></td>
</tr>
</tbody>
</table>

**Table** **5**: Functional Requirements IAT Issuing
