# Gaia-X TC WG Identity & Access Management - Meeting notes for 14/02/2023

## Compliance reminder
We adhere to the compliance document of Gaia-X: https://community.gaia-x.eu/apps/files/?dir=/AISBL/Legal%20Documents%20(All)/Code%20of%20Conduct&fileid=14648642
This means, joing the meeting means agreement to these terms:

* No price-fixing.
* No market or customer allocation.
* No output restrictions.
* No agreement on or exchange of competively sensitive business information.

## Agenda
* Compliance reminder.
* Last minutes.
* Input to the architecture document. What do we want to contribute?
  * https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/issues/62
  * https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/issues/52
* SWOT analysis
  * https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/issues/54
* Current status of the TRAIN issue
  * https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/issues/60
* Integrating principals (personal identities)
  * https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/issues/63
* General trust model for catalog interaction


## Minutes

* The group gets sidetracked by a multitute of issues and topics. We will try to focus on a few issues at a time. 
* TRAIN is going to be presented in a short pitch on the Technical Commitee. 

### Closed issues and decisions

* The group agrees to focus on the integration of OIDC4VP and SIOPv2 (so the protocols based on OIDC) without excluding other compatible protocols or methods, closing https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/issues/61.

### Open issues and questions :
* 

### Discussion and proposals
* Gerd will try to clean up the issue tracking board. 

### Special note 

