# Gaia-X TC WG Identity & Access Management - Meeting notes for 07/06/2022

## Minutes

### Closed issues and decisions

* [Issue 16 : Root of trust](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/16) : between the three proposed models, we select the model #3, the distributed trust model.
* [Issue 12 : RBAC vs ABAC](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/12) : we select ABAC (Attribute Based Access Control)


### Open issues and questions :

* [Issue 15 : IAM Vocabulary](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/15) : See comments in the issue.
* [Issue 13 : Do we need to promote a policy langage in the specification ?](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/13), the response is **yes**, now we have to chose them, please vote in the issue.


### Discussion and proposals
* [OpenID Connect compatibility] About SSI - OpenID Connect bridge, Berthold sent us a [link](https://gitlab.com/gaia-x/data-infrastructure-federation-services/authenticationauthorization/-/tree/main) to the implementation made by ECO (compatible with Keycloak).
* [OpenID Connect compatibility] Berthold suggest to setup a demonstration with development team for next week : TO BE CONFIRMED.
